$(document).ready(function(){

    //var localUri = window.location;
    //console.log(window.location);

    $('.go-reg').click(function(){
        window.open('http://localhost:63342/FINAL/final/registr.html');
    });

    $('.btn.before-log').click(function(){
        $('.after-log').removeClass("hide");
        $('.before-log').addClass("hide");
    })
    $('.btn.after-log').click(function(){
        $('.before-log').removeClass("hide");
        $('.after-log').addClass("hide");
    })

    $('.crt-post').click(function(){
        $('.create-post').toggleClass('hide');
    });

    $('.crt-comm').click(function(){
        $('.create-comments').toggleClass('hide');
    });

    $('#publish_post').bind('click',function(){
        $.ajax({
            url: 'http://spjs.izotov.tk/api/posts',
            method: 'POST',
            data: {
                title: $('#title').val(),
                description: $('#content_post').val(),
                user_id: 2
            },
            dataType: 'jsonp',
            crossDomain: true
        }).then(function (data) {
            if (data.error) {
            }
            else console.log(data);
        });
        return false;
    });

    $('').bind('click',function(){
        $.ajax({
            url: 'http://spjs.izotov.tk/api/posts',
            dataType: 'jsonp',
            crossDomain: true
        }).then(function(data){
            if(data.error){}
            else {
                $.each(data.Users, function(){
                    console.log(this.title);
                    console.log(this.discription);
                })
            }
        });
        return false;

    });

    var formButtons = $('.btn');
    var baseUri = 'http://spjs.izotov.tk/api/';

    var action = function(type){
        switch(type){
            case 'register':
                postUser();
                break;
            case 'list':
                getUser();
                break;
        }
        return false;
    }

    $(formButtons).on('click', function(e){
        action($(this).attr('data-buttonact'));
        e.preventDefault();
    });


    var postUser = function(){
        sendResponse('POST', getRegisterFormData, 'users', function(){return false;});
    }

    var getRegisterFormData = function(){
        var data = {};
        var ev = $('#email').val();
        var pv = $('#password').val();
        data = {
            email: ev,
            password: pv
        }
        return data;
    }

    var getUser = function(){
        sendResponse('GET', {}, 'users', renderUsers);
    }

    var getPost = function(){
        sendResponse('GET', {}, 'posts', renderPosts);
    }

    var sendResponse = function(type, data, entity, callback){
        if(!data)
            var ajaxData = {};
        var ajaxDataType = 'json';
        switch(type){
            case 'POST':
                ajaxData = data();
                break;
            case 'GET':
                break;
            case 'PUT':
                break;
            case 'DELETE':
                break;
        }

        $.ajax({
            url: baseUri+entity,
            dataType: ajaxDataType,
            crossDomain: true,
            type: type,
            data: ajaxData,
        }).then(function(data){
            if(data.error){}
            else {
                callback(data);
            }
        });

        return false;
    }
    var renderUsers = function (resp){
        if($.isArray(resp.Users)){
            $("#information").text("Information:   List of Users");
            console.log(resp);
            $.each(resp.Users, function(){
                $("<div></div>").addClass("row").insertBefore(".container-fluid").append(
                    $("<div></div>").addClass("col-xs-1").html(" User: "),
                    $("<div></div>").addClass("col-xs-3").html("Email: "+this.user_email),
                    //$("<div></div>").addClass("col-xs-4").html("Password: "+this.user_password),
                    $("<div></div>").addClass("col-xs-4").html("Join data : "+this.user_join_date)
                );
            })
        }

    }

    var renderPosts = function (resp){
        if($.isArray(resp.Posts)){
            $.each(resp.Posts, function(){
                var addComForm = $("#add-comment").clone();
                var comForm = $(addComForm).attr('id', 'post_'+this.id);
                console.log(comForm);
                $("#post-list").addClass("row").append(
                    $("<div></div>").html(this.title),
                    $("<div></div>").html('<button class="toggle-comment-form" data-post-id="'+this.id+'">SHOW comments</button>'),
                    $(comForm)
                );
            });
            $('.toggle-comment-form').on('click', function(e){
                //console.log($('#post_'+$(this).attr("data-post-id")));
                var formId = $(this).attr('data-post-id');
                $('#post_'+formId).slideToggle();
                //e.preventDefault();
            });
            //console.log(resp.Posts);
        }
    }

    $(".form-horizontal").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },
            password_confirm: {
                required: true,
                equalTo: "#password"
            }

        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element.form).find("label[for=" + element.id + "]")
                .addClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element.form).find("label[for=" + element.id + "]")
                .removeClass(errorClass);
        }
    })

    getPost();

});
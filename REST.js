var mysql   = require("mysql");

function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes = function(router,connection,md5) {
    var self = this;
    router.get("/",function(req,res){
        res.json({"Message" : "Hello World !"});
    });

    router.get("/users",function(req,res){
        var query = "SELECT * FROM ??";
        var table = ["user"];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
            }
        });
    });

    router.get("/posts",function(req,res){

        if(req.query.limit && req.query.offset) {
            var query = "SELECT * FROM ?? LIMIT ? OFFSET ?";
            var table = ["post", parseInt(req.query.limit), parseInt(req.query.offset)];
        }
        else {
            var query = "SELECT * FROM ??";
            var table = ["post"];
        }
        query = mysql.format(query,table);
        //console.log(query);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Posts" : rows});
            }
        });
    });

    router.get("/users/:id",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["user","id",req.params.id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "User" : rows[0]});
            }
        });
    });

    router.get("/posts/:id",function(req,res){
        var query = "SELECT * FROM ?? WHERE post.id=?";
        var table = ["post", req.params.id];
        query = mysql.format(query,table);
        var sbQuery = "SELECT c.*, u.user_email as user_email FROM ?? as c LEFT JOIN `user` as u ON c.user_id=u.id WHERE c.post_id=?";
        var sbTable = ["comment", req.params.id];
        var rowsComm = [];
        sbQuery = mysql.format(sbQuery,sbTable);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                console.log(err);
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                connection.query(sbQuery,function(err,rowsComm) {
                    res.json({"Error": false, "Message": "Success", "Post": rows[0], "Comments": rowsComm});
                });
            }
        });
    });

    router.get("/comments/:post_id",function(req,res){
        var query = "SELECT c.*, u.user_email as user_email FROM ?? as c LEFT JOIN `user` as u ON c.user_id=u.id WHERE c.post_id=?";
        var table = ["comment", req.params.post_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                console.log(err);
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Comments" : rows});
            }
        });
    });

    router.post("/posts",function(req,res){
        var query = "INSERT INTO ??(??,??,??) VALUES (?,?,?)";
        var table = ["post","title","description","user_id",req.body.title,req.body.description,req.body.user_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Post added!"});
            }
        });
    });

    router.post("/comments/:post_id",function(req,res){
        var query = "INSERT INTO ??(??,??,??) VALUES (?,?,?)";
        var table = ["comment","body","user_id","post_id",req.body.body,req.body.user_id,req.body.post_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Comment added!"});
            }
        });
    });

    router.post("/users",function(req,res){

        var query = "INSERT INTO ??(??,??) VALUES (?,?)";
        var table = ["user","user_email","user_password",req.body.email,md5(req.body.password)];
        query = mysql.format(query,table);
        console.log(query);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "User added!"});
            }
        });
    });

    router.delete("/users/:email",function(req,res){
        var query = "DELETE from ?? WHERE ??=?";
        var table = ["user","user_email",req.params.email];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "User deleted!"});
            }
        });
    });
}

module.exports = REST_ROUTER;
